# CUB_CAPTIONS_RAW

All captions and filenames with punctuation for the CUB_200_2011 dataset. 

To load captions and filenames:

with open(cub_path, 'rb') as f:
    all_captions, all_filenames = pickle.load(f)
    